//Dom
const BalanceEl = document.getElementById("StartBalance");
const DebtEl = document.getElementById("DebtBalance");
const PayEl = document.getElementById("PayBalance");

const LoanButtonEl = document.getElementById("LoanButton");
const BankButtonEL = document.getElementById("BankButton");
const WorkButtonEL = document.getElementById("WorkButton");
const RepayButtonEL = document.getElementById("RepayButton");
const BuyButtonEL = document.getElementById("BuyButton");

const laptopDdEl = document.getElementById("laptop-dropdown");
const laptopListEl = document.getElementById("laptop-list");
const laptopspecsEl = document.getElementById("laptop-specs");
const laptopdescEl = document.getElementById("laptop-desc");
const laptoppriceEl = document.getElementById("laptop-price");
const laptopTitleEl = document.getElementById("laptop-title");


//Variables
let BankBalance = parseInt(200);
let hasLoan = false;
let debt = parseInt(0);
let PayBalance = parseInt(0);
let PayPctLoan = parseInt(0);
let laptopPrice = parseInt(0);
let selectedItem;
const products = [];
const RequestJson = new Request("https://noroff-komputer-store-api.herokuapp.com/computers");

//Render Functions
function renderLaptopFt(products) {
    for (const product of products) {
        const html = `            
                <option value="${product.id}">${product.title}</option>
        `;
        //Create new entry for product
        laptopListEl.insertAdjacentHTML("beforebegin", html)
    }
}

//Fetch
fetch(RequestJson)
    .then((response) => {
        return response.json();
    }).then((data) => {
        console.log(data)
        products.push(...data)
        renderLaptopFt(products);
    })

laptopDdEl.addEventListener("change", (event) => {
    const target = event.target;
    console.log(products[target.value]);
    laptopspecsEl.innerText = products[target.value - 1].specs.join("\n");
    laptopdescEl.innerText = products[target.value - 1].description;
    laptoppriceEl.innerText = products[target.value - 1].price;
    laptopPrice = products[target.value - 1].price;
    laptopTitleEl.innerText = products[target.value - 1].title;
    const laptopImageEl = document.getElementById("image").src = (("https://noroff-komputer-store-api.herokuapp.com/") + products[target.value - 1].image);
});


BuyButtonEL.addEventListener("click", () => {
    console.log(laptopPrice);
    if (laptopPrice > BankBalance) {
        alert("You don't have enough money!");
    } else {
        BankBalance -= laptopPrice;
        updateBalance(BankBalance);
        alert("Congratulation on your new computer!");
    }
});

//ButtonEvent   
WorkButtonEL.addEventListener("click", () => {
    updatePayBalance(PayBalance);
    PayBalance += 100;
    updatePayBalance(PayBalance);

});

RepayButtonEL.addEventListener("click", () => {
    if (PayBalance < 1) {
        alert("You don't have enough!");
        return;
    }
    if (hasLoan === true) {
        if (PayBalance > debt) {
            PayBalance -= debt;
            debt = 0;
            updateDebt(debt);
            BankBalance += PayBalance;
            PayBalance = 0;
            updateBalance(BankBalance);
            updatePayBalance(PayBalance);
            hasLoan = false;
        } else if (PayBalance < debt) {
            debt -= PayBalance;
            PayBalance = 0;
            updateBalance(BankBalance);
            updatePayBalance(PayBalance);
            updateDebt(debt);
        }
    } else {
        alert("You don't have a loan!");
    }

});


BankButtonEL.addEventListener("click", () => {
    if (hasLoan == true) {
        PayPctLoan = PayBalance * 0.10;
        PayBalance -= PayPctLoan;
        if (PayPctLoan > debt) {
            PayPctLoan -= debt;
            debt = 0;
            hasLoan = false;
            BankBalance += (PayBalance + PayPctLoan);
            PayBalance = 0;
        } else {
            debt -= PayPctLoan;
            BankBalance += PayBalance;
            PayBalance = 0;
        }
    } else {
        BankBalance += PayBalance;
        PayBalance = 0;
    }
    updateDebt(debt);
    updateBalance(BankBalance);
    updatePayBalance(PayBalance);
});



LoanButtonEl.addEventListener("click", () => {
    updateBalance(BankBalance);
    if (hasLoan == false) {
        let loanAmmount = parseInt(prompt("Get a loan today! How much do you want?", "0"));
        if (loanAmmount >= (BankBalance * 2) || !loanAmmount) {
            alert("You cannot get loan!")
        }
        else {
            BankBalance += loanAmmount;
            debt += loanAmmount;
            updateDebt(debt);
            updateBalance(BankBalance);
            hasLoan = true;
        }

    } else {
        alert("You already have a loan!")
    }
});

//Update
function updatePayBalance(number) {
    PayEl.innerHTML = number;
}

function updateDebt(number) {
    DebtEl.innerHTML = number;
}

function updateBalance(number) {
    BalanceEl.innerHTML = number;
}
